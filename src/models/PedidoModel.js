const { knex } = require('../../config/db');

const PEDIDO = 'pedido';
const HISTORICO = 'historico';
const USUARIO = 'usuario';
const SITUACAO = 'situacao';
const PRODUTO = 'produto';
const CARRINHO = 'carrinho';

class PedidoModel {
  static listarPedidos() {
    return knex
      .from(PEDIDO)
      .select(
        `${PEDIDO}.id`,
        `${USUARIO}.nome AS empresa`,
        `${SITUACAO}.descricao AS status`,
        `${HISTORICO}.atualizado`,
      )
      .innerJoin(USUARIO, `${USUARIO}.id`, `${PEDIDO}.id_usuario`)
      .innerJoin(HISTORICO, `${HISTORICO}.id_pedido`, `${PEDIDO}.id`)
      .innerJoin(SITUACAO, `${SITUACAO}.id`, `${HISTORICO}.id_situacao`)
      .where(knex.raw(`${HISTORICO}.id IN (SELECT MAX(${HISTORICO}.id) AS id FROM ${HISTORICO} GROUP BY ${HISTORICO}.id_pedido)`))
      .groupBy(`${PEDIDO}.id`)
      .orderBy(`${PEDIDO}.id`, 'desc');
  }

  static obterClientePedido({ id }) {
    return knex
      .from(PEDIDO)
      .distinct(`${PEDIDO}.id`)
      .first(
        `${USUARIO}.nome`,
        `${USUARIO}.cnpj`,
        `${USUARIO}.estado`,
        `${USUARIO}.telefone`,
        `${USUARIO}.cep`,
        `${USUARIO}.endereco`,
        `${USUARIO}.pais`,
        `${USUARIO}.comentario`,
        `${USUARIO}.criado`
      )
      .sum(`${PRODUTO}.preco AS preco`)
      .innerJoin(USUARIO, `${USUARIO}.id`, `${PEDIDO}.id_usuario`)
      .innerJoin(CARRINHO, `${CARRINHO}.id_pedido`, `${PEDIDO}.id`)
      .innerJoin(PRODUTO, `${PRODUTO}.id`, `${CARRINHO}.id_produto`)
      .where(`${PEDIDO}.id`, id);
  }

  static obterProdutoPedido({ id }) {
    return knex
      .from(CARRINHO)
      .select(
        `${PRODUTO}.nome`,
        `${PRODUTO}.preco`,
      )
      .innerJoin(PRODUTO, `${PRODUTO}.id`, `${CARRINHO}.id_produto`)
      .where(`${CARRINHO}.id_pedido`, id);
  }

  static obterHistoricoPedido({ id }) {
    return knex
      .from(HISTORICO)
      .select(
        `${HISTORICO}.id`,
        `${HISTORICO}.atualizado`,
        `${HISTORICO}.descricao`,
        `${SITUACAO}.descricao`
      )
      .innerJoin(SITUACAO, `${SITUACAO}.id`, `${HISTORICO}.id_situacao`)
      .where(`${HISTORICO}.id_pedido`, id)
      .orderBy(`${HISTORICO}.id`, 'desc');
  }
}

module.exports = PedidoModel;
