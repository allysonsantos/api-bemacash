
const PedidoModel = require('../models/PedidoModel');
const moment = require('moment-timezone');

const analisarItem = item => ({
    ...item,
    atualizado: moment.utc(item.atualizado).format('DD/MM/YYYY HH:mm:ss')
});

class PedidoService {
    static async listarPedidos() {
        const resultado = await PedidoModel.listarPedidos();
        return resultado ? resultado.map(analisarItem) : null;
    }

    static async obterClientePedido(data) {
        const usuario = await PedidoModel.obterClientePedido(data);
        const resultado = {
            ...usuario,
            criado: moment.utc(usuario.criado).format('DD/MM/YYYY HH:mm:ss')
        }
        return resultado ? resultado : null;
    }

    static async obterProdutoPedido(data) {
        const resultado = await PedidoModel.obterProdutoPedido(data);
        return resultado ? resultado : null;
    }

    static async obterHistoricoPedido(data) {
        const resultado = await PedidoModel.obterHistoricoPedido(data);
        return resultado ? resultado.map(analisarItem) : null;
    }
}

module.exports = PedidoService;
