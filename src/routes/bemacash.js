"use strict";

const express = require("express");
const router = express.Router();
const PedidoController = require("../controllers/PedidoController");

// Rota para buscar a lista de pedidos
router.get("/listarpedidos", PedidoController.listarPedidos);
// Rota para buscar os dados do cliente do pedido
router.get("/obterclientepedido/:id", PedidoController.obterClientePedido);
// Rota para buscar os produtos do cliente do pedido
router.get("/obterprodutopedido/:id", PedidoController.obterProdutoPedido);
// Rota para buscar os histórico do cliente do pedido
router.get("/obterhistoricopedido/:id", PedidoController.obterHistoricoPedido);

module.exports = router
