const moment = require('moment-timezone');

class Datetime {
  static toUnixEpoch(dateString) {
    return moment.utc(dateString).valueOf();
  }

  static now() {
    return moment().utc();
  }

  static toMoment(date, format) {
    return moment(date, format, true);
  }

  static nowAddSeconds(seconds) {
    return moment().add(seconds, 'seconds');
  }

  static isBetweenOneMonth(startDate, dateToCompare = moment()) {
    return moment(dateToCompare).isBetween(startDate, moment(startDate).add(30, 'days').endOf('day'));
  }
}

module.exports = Datetime;
