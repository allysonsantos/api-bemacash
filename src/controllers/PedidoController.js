const PedidoService = require('../services/PedidoService');

class PedidoController {
    static async listarPedidos(req, res) {
        try {
            const resultado = await PedidoService.listarPedidos({ ...req.params, ...req.query });
            return res.status(200).send(resultado);
        } catch (erro) {
            console.log(erro);
            return res.status(500).json({ status: "false", message: "Não foi possível buscar a lista de pedidos!" }).end();
        }
    }

    static async obterClientePedido(req, res) {
        try {
            const cliente = await PedidoService.obterClientePedido({ ...req.params, ...req.query });

            return res.status(200).send(cliente);
        } catch (erro) {
            console.log(erro);
            return res.status(500).json({ status: "false", message: "Não foi possível buscar os dados do cliente!" }).end();
        }
    }

    static async obterProdutoPedido(req, res) {
        try {
            const produto = await PedidoService.obterProdutoPedido({ ...req.params, ...req.query });

            return res.status(200).send(produto);
        } catch (erro) {
            console.log(erro);
            return res.status(500).json({ status: "false", message: "Não foi possível buscar o(s) produto(s) do pedido!" }).end();
        }
    }

    static async obterHistoricoPedido(req, res) {
        try {
            const historico = await PedidoService.obterHistoricoPedido({ ...req.params, ...req.query });

            return res.status(200).send(historico);
        } catch (erro) {
            console.log(erro);
            return res.status(500).json({ status: "false", message: "Não foi possível buscar o histórico do pedido!" }).end();
        }
    }
}

module.exports = PedidoController;
