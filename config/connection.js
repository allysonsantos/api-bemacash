const config = require("config");

module.exports = {
  client: 'mysql2',
  debug: false,
  connection: {
    host: config.get("mysql.host"),
    user: config.get("mysql.user"),
    password: config.get("mysql.password"),
    database: config.get("mysql.database"),
    supportBigNumbers: true,
    bigNumberStrings: true,
    multipleStatements: true,
    timezone: 'UTC',
    dateStrings: true,
  },
  pool: {
    min: parseInt(config.get("mysql.db_pool_min") || 0, 10),
    max: parseInt(config.get("mysql.db_pool_max") || 1, 10),
  },
};
