const connection = require('knex');
const config = require('./connection');

const wrapperConn = () => {
  return { knex: connection(config) };
};

module.exports = wrapperConn();
