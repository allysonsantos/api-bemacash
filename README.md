API - Bemacash
====================

Objetivo deste serviço é mostrar a utilização da API REST através do NodeJs. 

# Tecnologias utilizadas

*NPM - v6.10.3*

*Node - v8.10.0*

MySQL - v5.7.23

# Estrutura de pastas

* **bin** (é o gatilho para executar o serviço)
* **config** (responsável por definir configurações globais para o serviço e do banco de dados)
* **src**
    * **controllers** (responsável por controlar e direcionar as requisições para o serviço desejável)
    * **helpers** (contêm funcionalidades para auxiliar o desenvolvimento)
    * **images** (armazenar imagens do serviço e documentação)
    * **middlewares** (responsável por validação de campo, autenticação da requisição, etc. Porém, neste serviço está verificando apenas `header` da requisição)
    * **models** (responsável por manipulação de dados)    
    * **routes** (responsável pelas rotas da api)
    * **services** (responsável pelas regras de negócio obtendo os dados a partir da camada `model`)

## Executar serviço

### Etapa 1

```
git clone https://allysonsantos@bitbucket.org/allysonsantos/api-bemacash.git
```
```
cd api-bemacash
```
```
npm install
```

### Etapa 2

* Precisa fazer a importação do arquivo `dump.sql` que está na pasta `documents`.
* Após a importação do `dump.sql`, precisa editar o arquivo de configuração do sistema que se encontra no caminho: `config/default.json`. Você precisa apontar para seu banco de dados alterando os dados dentro de mysql:

```
{
    "server": {
        "port": 3050,
        "host": "0.0.0.0"
    },
    "mysql": {
        "host": "localhost",
        "user": "root",
        "password": "",
        "database": "bemacash",
        "db_pool_min": 2,
        "db_pool_max": 10
    },
    "version": 1
}
```

### Etapa 3

Após garantir as etapas 1 e 2. Você precisa somente executar o comando abaixo dentro da pasta `api-bemacash`, assim poderá utilizar API REST:

```
npm start
```

### Exemplos de requisição e retorno

![picture](src/images/listarpedidos.PNG)
![picture](src/images/obterclientepedido.PNG)
![picture](src/images/obterprodutopedido.PNG)
![picture](src/images/obterhistoricopedido.PNG)