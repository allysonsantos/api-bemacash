SELECT 
    P.id AS pedido,
    U.nome AS empresa,
    S.descricao AS status,
    H.atualizado
FROM
    pedido AS P
        INNER JOIN
    usuario AS U ON P.id_usuario = U.id
        INNER JOIN
    historico AS H ON P.id = H.id_pedido
        INNER JOIN
    situacao AS S ON H.id_situacao = S.id
WHERE
    H.id IN (SELECT 
            MAX(id) AS id
        FROM
            historico
        GROUP BY id_pedido)
GROUP BY P.id
ORDER BY P.id DESC