SELECT 
    H.id, H.atualizado, H.descricao, S.descricao
FROM
    historico AS H
        INNER JOIN
    situacao AS S ON H.id_situacao = S.id
WHERE
    H.id_pedido = 1