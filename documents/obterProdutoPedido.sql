SELECT 
	C.id_pedido,
    P.nome,
    P.preco
FROM
    carrinho AS C
        INNER JOIN
    produto AS P ON C.id_produto = P.id
WHERE
    C.id_pedido = 1