SELECT 
	DISTINCT P.id,
    U.nome,
    U.cnpj,
    U.estado,
    U.telefone,
    U.cep,
    U.endereco,
    U.pais,
    U.comentario,
    U.criado,
    SUM(PR.preco) AS preco
FROM
    pedido AS P
        INNER JOIN
    usuario AS U ON P.id_usuario = U.id
        INNER JOIN
	carrinho AS C ON P.id = C.id_pedido
		INNER JOIN
	produto AS PR ON C.id_produto = PR.id
WHERE
    P.id = 1