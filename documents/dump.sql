-- -----------------------------------------------------
-- Schema bemacash
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bemacash` DEFAULT CHARACTER SET utf8 ;
USE `bemacash` ;

--
-- Estrutura da tabela `carrinho`
--

DROP TABLE IF EXISTS `carrinho`;
CREATE TABLE IF NOT EXISTS `bemacash`.`carrinho` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_pedido` bigint(11) NOT NULL,
  `id_produto` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_carrinho_pedido_idx` (`id_pedido`),
  KEY `fk_carrinho_produto1_idx` (`id_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `carrinho`
--

INSERT INTO `carrinho` (`id`, `id_pedido`, `id_produto`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 2, 6),
(4, 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico`
--

DROP TABLE IF EXISTS `historico`;
CREATE TABLE IF NOT EXISTS `bemacash`.`historico` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `atualizado` timestamp NOT NULL,
  `id_pedido` bigint(11) NOT NULL,
  `id_situacao` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_historico_pedido1_idx` (`id_pedido`),
  KEY `fk_historico_situacao1_idx` (`id_situacao`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `historico`
--

INSERT INTO `historico` (`id`, `descricao`, `atualizado`, `id_pedido`, `id_situacao`) VALUES
(1, 'aaaa', '2019-08-30 03:00:00', 1, 1),
(2, 'bbbbb', '2019-08-30 03:01:00', 1, 2),
(3, 'aaaa', '2019-08-30 03:00:00', 2, 1),
(4, 'bbbb', '2019-08-30 03:02:00', 2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

DROP TABLE IF EXISTS `pedido`;
CREATE TABLE IF NOT EXISTS `bemacash`.`pedido` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `criado` timestamp NOT NULL,
  `id_usuario` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pedido_usuario1_idx` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pedido`
--

INSERT INTO `pedido` (`id`, `criado`, `id_usuario`) VALUES
(1, '2019-08-30 03:00:00', 1),
(2, '2019-08-30 03:00:00', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

DROP TABLE IF EXISTS `produto`;
CREATE TABLE IF NOT EXISTS `bemacash`.`produto` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `preco` float NOT NULL,
  `criado` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `nome`, `preco`, `criado`) VALUES
(1, 'Tablet', 5000, '2019-08-27 03:00:00'),
(2, 'Impressora', 1000, '2019-08-19 03:00:00'),
(3, 'Monitor', 1500, '2019-08-07 03:00:00'),
(6, 'Notebook', 4000, '2019-08-01 03:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacao`
--

DROP TABLE IF EXISTS `situacao`;
CREATE TABLE IF NOT EXISTS `bemacash`.`situacao` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `situacao`
--

INSERT INTO `situacao` (`id`, `descricao`) VALUES
(1, 'Em processo'),
(2, 'Aprovado'),
(3, 'Reprovado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `bemacash`.`usuario` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `cnpj` varchar(14) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  `cep` varchar(8) NOT NULL,
  `endereco` varchar(120) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `comentario` text,
  `criado` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `cnpj`, `estado`, `telefone`, `cep`, `endereco`, `pais`, `comentario`, `criado`) VALUES
(1, 'Empresa A', '16935235000105', 'rs', '5133671354', '91240550', 'Ada Vaz Cabeda, 400', 'Brasil', NULL, '2019-08-29 03:00:00'),
(2, 'Empresa B', '69397194000107', 'rs', '5134923375', '91150303', 'Rua João Wilson de Almeida, 30', 'Brasil', NULL, '2019-08-28 03:00:00');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `carrinho`
--
ALTER TABLE `carrinho`
  ADD CONSTRAINT `fk_carrinho_pedido` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_carrinho_produto1` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `historico`
--
ALTER TABLE `historico`
  ADD CONSTRAINT `fk_historico_pedido1` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_historico_situacao1` FOREIGN KEY (`id_situacao`) REFERENCES `situacao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fk_pedido_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;
